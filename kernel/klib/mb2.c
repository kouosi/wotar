#include <kernel/klib/kprintf.h>
#include <kernel/klib/mb2.h>
#include <stdint.h>

void parse_mb2_info (uint32_t mb2_addr)
{
    if (mb2_addr & 0x7) {
        kprintf (kerr, "Unaligned MBI: 0x%x\n", mb2_addr);
        while (1)
            ;
    }
    struct mb2_tag_struct *main_tag = (struct mb2_tag_struct *) (mb2_addr + 8);
    while (main_tag->type != MB2_TAG_END) {
        switch (main_tag->type) {
        case MB2_TAG_MEM_MAP: {
            struct mb2_tag_mem_map_entry *mem_map = ((struct mb2_tag_mem_map *) main_tag)->entries;
            while ((uint8_t *) mem_map < (uint8_t *) main_tag + main_tag->size) {
                kprintf (kmsg, "\tbase_addr = 0x%x%x, length = 0x%x%x, type = %x\n",
                         (uint32_t) (mem_map->base_addr >> 32),
                         (uint32_t) (mem_map->base_addr & 0xffffffff),
                         (uint32_t) (mem_map->len >> 32), (uint32_t) (mem_map->len & 0xffffffff),
                         (uint32_t) (mem_map->type));
                mem_map = (struct mb2_tag_mem_map_entry *) ((unsigned long) mem_map +
                                                            ((struct mb2_tag_mem_map *) main_tag)
                                                                ->entry_size);
            }
        } break;
        case MB2_TAG_BASIC_MEM_INFO: {
            kprintf (kmsg, "mem_lower = %dKB, mem_upper = %dKB\n",
                     ((struct mb2_tag_basic_mem_info *) main_tag)->mem_lower,
                     ((struct mb2_tag_basic_mem_info *) main_tag)->mem_upper);
        } break;
        }
        main_tag = (struct mb2_tag_struct *) ((uint8_t *) main_tag + ((main_tag->size + 7) & ~7));
    }
}

