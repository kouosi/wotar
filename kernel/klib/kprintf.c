/**
 * FIXME: this is just stub implementation for serial loging.
 * TODO: add support to print with kprintf other than serial port
 */
#include <kernel/drivers/serial.h>
#include <kernel/klib/kprintf.h>
#include <kernel/klib/kstring.h>
#include <stdarg.h>

void (*print_ch) (char) = &serial_write;

static inline void kprint_char (char ch)
{
    print_ch (ch);
}

static inline void kprint_str (const char *str)
{
    while (*str) {
        kprint_char (*str);
        str++;
    }
}

static inline void kprint_dec (int num)
{
    if (num < 0) {
        kprint_char ('-');
        num = -num;
    }
    if (num == 0) {
        kprint_char ('0');
        return;
    }
    char buffer[16];
    int index = 0;
    while (num > 0) {
        buffer[index] = '0' + (num % 10);
        num /= 10;
        index++;
    }
    buffer[index] = '\0';
    char reversed[16];
    int rev_index = 0;
    for (int i = index - 1; i >= 0; i--) {
        reversed[rev_index] = buffer[i];
        rev_index++;
    }
    reversed[rev_index] = '\0';
    kprint_str (reversed);
}

static inline void kprint_hex (int num)
{
    char buffer[9];    // Enough for 8 hex digits and a null terminator
    itoa (buffer, num, 16);
    kprint_str (buffer);
}

static inline void kprint_ptr (void *ptr)
{
    char buf[20];
    itoa (buf, (unsigned int) ptr, 16);
    kprint_str ("0x");
    kprint_str (buf);
}

void kvprintf (const char *fmt, va_list args)
{
    while (*fmt) {
        if (*fmt == '%') {
            fmt++;
            switch (*fmt) {
            case 'c':
                kprint_char (va_arg (args, int));
                break;
            case 's':
                kprint_str (va_arg (args, const char *));
                break;
            case 'd':
            case 'i':
                kprint_dec (va_arg (args, int));
                break;
            case 'x':
            case 'X':
                kprint_hex (va_arg (args, unsigned int));
                break;
            case 'p':
                kprint_ptr (va_arg (args, void *));
                break;
            default:
                kprint_char (*fmt);
                break;
            }
        } else {
            kprint_char (*fmt);
        }
        fmt++;
    }
}

void kprintf (kstream print_stream, const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);
    if (print_ch == &serial_write) {
        if (print_stream == kerr) {
            kprint_str ("\033[1;31m");
        } else if (print_stream == kwarn) {
            kprint_str ("\033[1;93m");
        } else if (print_stream == ksucc) {
            kprint_str ("\033[1;32m");
        } else if (print_stream == kmsg) {
            kprint_str ("\033[1;97m");
        }
        kvprintf (fmt, args);
        kprint_str ("\033[0m");
    } else {
        kvprintf (fmt, args);
    }
    va_end (args);
}

