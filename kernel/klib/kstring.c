#include <kernel/klib/kstring.h>
#include <stdbool.h>

char *itoa (char *buffer, int value, int base)
{
    if (base < 2 || base > 16) {
        *buffer = '\0';
        return buffer;
    }

    bool is_negative = false;
    int index        = 0;

    // Handle the sign for base 10
    if (base == 10 && value < 0) {
        is_negative = true;
        value       = -value;
    }

    // Build the string in reverse order
    do {
        int remainder   = value % base;
        buffer[index++] = (remainder < 10) ? ('0' + remainder) : ('A' + remainder - 10);
        value /= base;
    } while (value > 0);

    // Add the negative sign for base 10
    if (is_negative) {
        buffer[index++] = '-';
    }

    // Null-terminate the string
    buffer[index] = '\0';

    // Reverse the string in place
    int start = 0;
    int end   = index - 1;
    while (start < end) {
        char temp     = buffer[start];
        buffer[start] = buffer[end];
        buffer[end]   = temp;
        start++;
        end--;
    }

    return buffer;
}

