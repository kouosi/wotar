#include <kernel/drivers/serial.h>
#include <kernel/system.h>

/**
 * TODO: option to init other ports also for not hard code to port i
 */

void serial_init (void)
{
    outb (PORT_COM1 + 1, 0x00);    // Disable all interrupts
    outb (PORT_COM1 + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    outb (PORT_COM1 + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
    outb (PORT_COM1 + 1, 0x00);    // hi byte
    outb (PORT_COM1 + 3, 0x03);
    outb (PORT_COM1 + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    outb (PORT_COM1 + 4, 0x0B);    // IRQs enabled, RTS/DSR set
    outb (PORT_COM1 + 4, 0x0F);    // set it in normal operation mode
}

uint8_t serial_read (void)
{
    while (! serial_received ())
        ;
    return inb (PORT_COM1);
}

void serial_write (char ch)
{
    while (! is_transmit_empty ())
        ;
    outb (PORT_COM1, ch);
}

uint32_t serial_received (void)
{
    return inb (PORT_COM1 + 5) & 1;
}

uint32_t is_transmit_empty (void)
{
    return inb (PORT_COM1 + 5) & 0x20;
}

void serial_puts (const char *str)
{
    while (*str) {
        serial_write (*str);
        str++;
    }
}

