#include <kernel/drivers/serial.h>
#include <kernel/i686/gdt.h>
#include <kernel/i686/idt.h>
#include <kernel/klib/kprintf.h>
#include <kernel/klib/mb2.h>
#include <stdint.h>

void kernel_init (uint32_t mb2_addr, uint32_t magic)
{
    serial_init ();
    serial_puts ("Serial Init Success!\n");
    if (magic != MB2_BOOTLOADER_MAGIC) {
        kprintf (kerr, "Invalid Magic Number: 0x%x", magic);
        while (1)
            ;
    }
    parse_mb2_info (mb2_addr);
    gdt_init ();
    serial_puts ("GDT Init Success!\n");
    idt_init ();
    serial_puts ("IDT Init Success!\n");

    while (1)
        ;
}

