#include <kernel/i686/idt.h>
#include <kernel/i686/irq.h>
#include <kernel/klib/kprintf.h>
#include <kernel/system.h>
#include <stdint.h>

#define PIC1_COMMAND 0x20
#define PIC2_COMMAND 0xA0
#define PIC_EOI      0x20

void irq_ack (uint32_t irq_no)
{
    if (irq_no >= 8) {
        outb (PIC2_COMMAND, PIC_EOI);
    }
    outb (PIC1_COMMAND, PIC_EOI);
}

void irq_init (void)
{
    pit_init ();
}

