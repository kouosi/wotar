#include <kernel/i686/gdt.h>
#include <stdint.h>

gdt_ptr_s gdt_ptr;
gdt_entry_s gdt_entry[5];

static inline void gdt_install (uint32_t m_gdt_ptr)
{
    asm volatile ("lgdt (%0)" ::"r"(m_gdt_ptr));
    asm volatile ("jmp $0x08,$foo \n"
                  "foo: \n");    // gdt far jump stub
}

void gdt_set_entry (int index, uint32_t base, uint32_t limit, uint32_t access_byte,
                    uint8_t granularity)
{
    gdt_entry_s *desc = &gdt_entry[index];
    desc->limit_lower = limit & 0xffff;
    desc->base_lower  = base & 0xffff;
    desc->base_middle = (base >> 16) & 0xff;
    desc->access_byte = access_byte;
    desc->granularity = (limit >> 16) & 0x0f;
    desc->granularity |= (granularity & 0xf0);
    desc->base_high = (base >> 24) & 0xff;
}

void gdt_init (void)
{
    gdt_ptr.limit = sizeof (gdt_entry) - 1;
    gdt_ptr.base  = (uint32_t) gdt_entry;

    /* NULL segment */
    gdt_set_entry (0, 0, 0, 0, 0);
    /* code segment */
    gdt_set_entry (1, 0, 0xffffffff, 0x9a, 0xcf);
    /* data segment */
    gdt_set_entry (2, 0, 0xffffffff, 0x92, 0xcf);
    /* user code segment */
    gdt_set_entry (3, 0, 0xffffffff, 0xfa, 0xcf);
    /* user data segment */
    gdt_set_entry (4, 0, 0xffffffff, 0xf2, 0xcf);

    gdt_install ((uint32_t) &gdt_ptr);
}

