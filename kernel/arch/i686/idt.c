#include <kernel/drivers/serial.h>
#include <kernel/i686/idt.h>
#include <kernel/i686/irq.h>
#include <kernel/i686/isr.h>
#include <kernel/klib/kprintf.h>

idt_ptr_s idt_ptr;
idt_entry_s idt_entry[TOTAL_IDT_ENTRY];

void default_handler (interrupt_frame_s *frame) __attribute__ ((interrupt, noreturn));

void default_handler (interrupt_frame_s *frame)
{
    kprintf (kerr, "Unknown Exception occured!\n");
    kprintf (kerr, "\tCS: 0x%x\n\tFLAGS: 0x%x\n\tIP: 0x%x\n\tSP: 0x%x\n\tSS: 0x%x\n", frame->cs,
             frame->flags, frame->ip, frame->sp, frame->ss);
    asm volatile ("cli \n"
                  "hlt \n");
    while (1)
        ;
}

static inline void idt_install (uint32_t m_idt_ptr)
{
    asm volatile ("lidt (%0)" ::"r"(m_idt_ptr));
    asm volatile ("sti");
}

void p_idt_set_entry (uint32_t interrupt_no, uint32_t handler, uint16_t selector, uint8_t flag)
{
    idt_entry_s *disc     = &idt_entry[interrupt_no];
    disc->offset_lower    = handler & 0xffff;
    disc->selector        = selector;
    disc->zero            = 0;
    disc->type_attributes = flag | 0x60;
    disc->offset_higher   = (handler >> 16) & 0xffff;
}

void idt_add_entry (int interrupt_no, uint32_t handler, uint16_t selector, uint8_t flag)
{
    if (interrupt_no > 31) {
        p_idt_set_entry (interrupt_no, handler, selector, flag);
    } else {
        // TODO: Add Debug options
    }
}

void idt_init (void)
{
    idt_ptr.limit = sizeof (idt_entry) - 1;
    idt_ptr.base  = (uint32_t) idt_entry;

    for (uint32_t i = 0; i < TOTAL_IDT_ENTRY; i++) {
        p_idt_set_entry (i, (uint32_t) default_handler, 0x08, 0x8e);
    }

    // Exceptions Handlers
    p_idt_set_entry (0, (uint32_t) isr_vector_0, 0x08, 0x8e);
    p_idt_set_entry (1, (uint32_t) isr_vector_1, 0x08, 0x8e);
    p_idt_set_entry (2, (uint32_t) isr_vector_2, 0x08, 0x8e);
    p_idt_set_entry (3, (uint32_t) isr_vector_3, 0x08, 0x8e);
    p_idt_set_entry (4, (uint32_t) isr_vector_4, 0x08, 0x8e);
    p_idt_set_entry (5, (uint32_t) isr_vector_5, 0x08, 0x8e);
    p_idt_set_entry (6, (uint32_t) isr_vector_6, 0x08, 0x8e);
    p_idt_set_entry (7, (uint32_t) isr_vector_7, 0x08, 0x8e);
    p_idt_set_entry (8, (uint32_t) isr_vector_8, 0x08, 0x8e);
    p_idt_set_entry (9, (uint32_t) isr_vector_9, 0x08, 0x8e);
    p_idt_set_entry (10, (uint32_t) isr_vector_10, 0x08, 0x8e);
    p_idt_set_entry (11, (uint32_t) isr_vector_11, 0x08, 0x8e);
    p_idt_set_entry (12, (uint32_t) isr_vector_12, 0x08, 0x8e);
    p_idt_set_entry (13, (uint32_t) isr_vector_13, 0x08, 0x8e);
    p_idt_set_entry (14, (uint32_t) isr_vector_14, 0x08, 0x8e);
    p_idt_set_entry (15, (uint32_t) isr_vector_15, 0x08, 0x8e);
    p_idt_set_entry (16, (uint32_t) isr_vector_16, 0x08, 0x8e);
    p_idt_set_entry (17, (uint32_t) isr_vector_17, 0x08, 0x8e);
    p_idt_set_entry (18, (uint32_t) isr_vector_18, 0x08, 0x8e);
    p_idt_set_entry (19, (uint32_t) isr_vector_19, 0x08, 0x8e);
    p_idt_set_entry (20, (uint32_t) isr_vector_20, 0x08, 0x8e);
    p_idt_set_entry (21, (uint32_t) isr_vector_21, 0x08, 0x8e);
    p_idt_set_entry (22, (uint32_t) isr_vector_22, 0x08, 0x8e);
    p_idt_set_entry (23, (uint32_t) isr_vector_23, 0x08, 0x8e);
    p_idt_set_entry (24, (uint32_t) isr_vector_24, 0x08, 0x8e);
    p_idt_set_entry (25, (uint32_t) isr_vector_25, 0x08, 0x8e);
    p_idt_set_entry (26, (uint32_t) isr_vector_26, 0x08, 0x8e);
    p_idt_set_entry (27, (uint32_t) isr_vector_27, 0x08, 0x8e);
    p_idt_set_entry (28, (uint32_t) isr_vector_28, 0x08, 0x8e);
    p_idt_set_entry (29, (uint32_t) isr_vector_29, 0x08, 0x8e);
    p_idt_set_entry (30, (uint32_t) isr_vector_30, 0x08, 0x8e);
    p_idt_set_entry (31, (uint32_t) isr_vector_31, 0x08, 0x8e);

    pic_init ();
    idt_install ((uint32_t) &idt_ptr);
}
