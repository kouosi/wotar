#include <kernel/i686/idt.h>
#include <kernel/i686/irq.h>
#include <kernel/i686/isr.h>
#include <kernel/klib/kprintf.h>
#include <kernel/system.h>
#include <stdint.h>

#define PIT_FREQ   1193180
#define FREQ       128
#define PIT_SET    0x36
#define PIT_MASK   0xff
#define PIT_CHAN_0 0x40
#define PIT_CHAN_1 0x41
#define PIT_CHAN_2 0x42
#define PIT_MODE   0x43
#define IRQ_0      32

uint32_t a = 0;

void irq_timer (interrupt_frame_s *frame) __attribute__ ((interrupt));

void irq_timer (interrupt_frame_s *frame)
{
    ++a;
    kprintf (kerr, "%d\n", a);
    irq_ack (IRQ_0);
}

void pit_init (void)
{
    idt_add_entry (IRQ_0, (uint32_t) irq_timer, 0x08, 0x8e);
    /* Frequency */
    uint32_t d_freq = PIT_FREQ / FREQ;
    outb (PIT_MODE, PIT_SET);
    outb (PIT_CHAN_0, d_freq & PIT_MASK);
    outb (PIT_CHAN_0, (d_freq >> 8) & PIT_MASK);
    kprintf (ksucc, "PIT init with %d HZ\n", FREQ);
}

