#include <kernel/i686/irq.h>
#include <kernel/system.h>
#include <stdint.h>

#define PIC1         0x20
#define PIC1_COMMAND PIC1
#define PIC1_DATA    (PIC1 + 1)
#define PIC1_OFFSET  0x20

#define PIC2         0xA0
#define PIC2_COMMAND PIC2
#define PIC2_DATA    (PIC2 + 1)
#define PIC2_OFFSET  0x28

#define ICW1_ICW4      0x01 /* Indicates that ICW4 will be present */
#define ICW1_SINGLE    0x02 /* Single (cascade) mode */
#define ICW1_INTERVAL4 0x04 /* Call address interval 4 (8) */
#define ICW1_LEVEL     0x08 /* Level triggered (edge) mode */
#define ICW1_INIT      0x10 /* Initialization - required! */

#define ICW4_8086       0x01 /* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO       0x02 /* Auto (normal) EOI */
#define ICW4_BUF_SLAVE  0x08 /* Buffered mode/slave */
#define ICW4_BUF_MASTER 0x0C /* Buffered mode/master */
#define ICW4_SFNM       0x10 /* Special fully nested (not) */

static inline void io_wait (void)
{
    outb (0x80, 0);
}

void pic_remap (int offset1, int offset2)
{
    /* Save masks */
    uint8_t a1 = inb (PIC1_DATA);
    uint8_t a2 = inb (PIC2_DATA);

    outb (PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
    io_wait ();
    outb (PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
    io_wait ();

    /* Remap PIC */
    outb (PIC1_DATA, offset1);
    io_wait ();
    outb (PIC2_DATA, offset2);
    io_wait ();

    /* Config slave PIC at IRQ2 */
    outb (PIC1_DATA, 0x4);
    io_wait ();
    outb (PIC2_DATA, 0x2);
    io_wait ();

    /* Use 8086 mode */
    outb (PIC1_DATA, ICW4_8086);
    io_wait ();
    outb (PIC2_DATA, ICW4_8086);
    io_wait ();

    /* Restore saved mask */
    outb (PIC1_DATA, a1);
    outb (PIC2_DATA, a2);
}

void pic_init (void)
{
    pic_remap (PIC1_OFFSET, PIC2_OFFSET);
    irq_init ();
}

