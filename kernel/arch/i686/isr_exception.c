#include <kernel/i686/isr.h>
#include <kernel/klib/kprintf.h>

static inline void print_interrupt_frame (interrupt_frame_s *frame)
{

    kprintf (kerr, "\tCS: 0x%x\n\tFLAGS: 0x%x\n\tIP: 0x%x\n\tSP: 0x%x\n\tSS: 0x%x\n", frame->cs,
             frame->flags, frame->ip, frame->sp, frame->ss);
    asm volatile ("cli \n"
                  "hlt\n");
}

void isr_vector_0 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Division By Zero!\n");
    print_interrupt_frame (frame);
}

void isr_vector_1 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Debug!\n");
    print_interrupt_frame (frame);
}

void isr_vector_2 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Non Maskable Interrupt!\n");
    print_interrupt_frame (frame);
}

void isr_vector_3 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Breakpoint!\n");
    print_interrupt_frame (frame);
}

void isr_vector_4 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Overflow!\n");
    print_interrupt_frame (frame);
}

void isr_vector_5 (interrupt_frame_s *frame)
{
    kprintf (kerr, "BOUND Range Exceeded!\n");
    print_interrupt_frame (frame);
}

void isr_vector_6 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Invalid Opcode!\n");
    print_interrupt_frame (frame);
}

void isr_vector_7 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Device Not Available (No Math Coprocessor)!\n");
    print_interrupt_frame (frame);
}

void isr_vector_8 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Double Fault!\n");
    print_interrupt_frame (frame);
}

void isr_vector_9 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Coprocessor Segment Overrun!\n");
    print_interrupt_frame (frame);
}

void isr_vector_10 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Invalid TSS!\n");
    print_interrupt_frame (frame);
}

void isr_vector_11 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Segment Not Present!\n");
    print_interrupt_frame (frame);
}

void isr_vector_12 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Stack-Segment Fault!\n");
    print_interrupt_frame (frame);
}

void isr_vector_13 (interrupt_frame_s *frame)
{
    kprintf (kerr, "General Protection!\n");
    print_interrupt_frame (frame);
}

void isr_vector_14 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Page Fault!\n");
    print_interrupt_frame (frame);
}

void isr_vector_15 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Unknown Interrupt (intel reserved)!\n");
    print_interrupt_frame (frame);
}

void isr_vector_16 (interrupt_frame_s *frame)
{
    kprintf (kerr, "x87 FPU Floating-Point Error (Math Fault)!\n");
    print_interrupt_frame (frame);
}

void isr_vector_17 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Alignment Check!\n");
    print_interrupt_frame (frame);
}

void isr_vector_18 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Machine Check!\n");
    print_interrupt_frame (frame);
}

void isr_vector_19 (interrupt_frame_s *frame)
{
    kprintf (kerr, "SIMD Floating-Point Exception!\n");
    print_interrupt_frame (frame);
}

void isr_vector_20 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Virtualization Exception!\n");
    print_interrupt_frame (frame);
}

void isr_vector_21 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 21\n");
    print_interrupt_frame (frame);
}

void isr_vector_22 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 22\n");
    print_interrupt_frame (frame);
}

void isr_vector_23 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 23\n");
    print_interrupt_frame (frame);
}

void isr_vector_24 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 24\n");
    print_interrupt_frame (frame);
}

void isr_vector_25 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 25\n");
    print_interrupt_frame (frame);
}

void isr_vector_26 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 26\n");
    print_interrupt_frame (frame);
}

void isr_vector_27 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 27\n");
    print_interrupt_frame (frame);
}

void isr_vector_28 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 28\n");
    print_interrupt_frame (frame);
}

void isr_vector_29 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 29\n");
    print_interrupt_frame (frame);
}

void isr_vector_30 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 30\n");
    print_interrupt_frame (frame);
}

void isr_vector_31 (interrupt_frame_s *frame)
{
    kprintf (kerr, "Reserved! Int no. 31\n");
    print_interrupt_frame (frame);
}

