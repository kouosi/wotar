ARCH		:= i686
BUILD_DIR	:= build
AS	:= $(ARCH)-elf-as
CC	:= $(ARCH)-elf-gcc
AS_FLAG	:=
CC_FLAG := -Wall -Wextra -Wpedantic -Wformat=2 -Wnested-externs -Wshadow \
           -Wno-unused-parameter -Wwrite-strings -Wstrict-prototypes \
           -Wold-style-definition -Wredundant-decls  -Wmissing-include-dirs
CC_FLAG	+= -std=gnu11 -ffreestanding -O2 -Iinclude -static -pie 	\
           -mgeneral-regs-only
LD_FLAG	:= -nostdlib -lgcc
QEMU	    := qemu-system-x86_64
QEMU_FLAG	:= -cdrom myos.iso -m 32 -display sdl -no-reboot -serial stdio -d int
C_SRCS	:= $(wildcard kernel/*.c kernel/*/*.c kernel/arch/$(ARCH)/*.c)
A_SRCS	:= $(wildcard kernel/*.S kernel/arch/$(ARCH)/*.S)
OBJS 	:= $(patsubst %.c, $(BUILD_DIR)/%.o, $(C_SRCS))
OBJS 	+= $(patsubst %.S, $(BUILD_DIR)/%.o, $(A_SRCS))

all: iso

iso: build
	@mkdir -p $(BUILD_DIR)/isodir/boot/grub
	@cp assets/grub.cfg $(BUILD_DIR)/isodir/boot/grub/grub.cfg
	@cp $(BUILD_DIR)/test.os $(BUILD_DIR)/isodir/boot/test.os
	@grub-mkrescue -o myos.iso $(BUILD_DIR)/isodir/

run: iso
	$(QEMU) $(QEMU_FLAG)

build: $(OBJS)
	$(CC) -T linker/linker.ld $(LD_FLAG) $(CC_FLAG) -o $(BUILD_DIR)/test.os $(OBJS)

build_dir:
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(dir $(OBJS))

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)

$(BUILD_DIR)/%.o: %.c | build_dir
	$(CC) $(CC_FLAG) -c -o $@ $<

$(BUILD_DIR)/%.o: %.S | build_dir
	$(AS) $(AS_FLAG) -c -o $@ $<

