# Wotar
My First Attempt to do osdev.

## build
Assuming you have `i686-elf-gcc` crosscompile and `xorriso` `qemu` for iso.
```bash
make all
```
which builds `myos.iso`

## State
- [x] GDT
- [x] IDT
- [ ] Paging
- [ ] Heap

