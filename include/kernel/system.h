#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include <stdint.h>

void outb (uint16_t _port, uint8_t _val);
uint8_t inb (uint16_t _port);
uint16_t inw (uint16_t _port);
void outw (uint16_t _port, uint16_t _data);
uint32_t inl (uint16_t _port);
void outl (uint16_t _port, uint32_t _data);

#endif /* __SYSTEM_H__ */

