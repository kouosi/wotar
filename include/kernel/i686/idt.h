#ifndef __IDT_H__
#define __IDT_H__

#include <stdint.h>

#define TOTAL_IDT_ENTRY 256

typedef struct {
    uint16_t offset_lower;
    uint16_t selector;
    uint8_t zero;
    uint8_t type_attributes;
    uint16_t offset_higher;
} __attribute__ ((packed)) idt_entry_s;

typedef struct {
    uint16_t limit;
    uint32_t base;
} __attribute__ ((packed)) idt_ptr_s;

void idt_init (void);
void idt_add_entry (int interrupt_no, uint32_t handler, uint16_t selector, uint8_t flag);

#endif /* __IDT_H__ */

