#ifndef __IRQ_H__
#define __IRQ_H__

#include <stdint.h>

/* PIC */
void pic_init (void);

/* IRQ Functions */
void irq_init (void);
void irq_ack (uint32_t irq_no);

/* IRQS */
void pit_init (void);

#endif /* __IRQ_H__ */

