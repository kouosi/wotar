#ifndef __ISR_H__
#define __ISR_H__

#include <stdint.h>

typedef struct {
    uint16_t ip;
    uint16_t cs;
    uint16_t flags;
    uint16_t sp;
    uint16_t ss;
} __attribute__ ((packed)) interrupt_frame_s;

void isr_vector_0 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_1 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_2 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_3 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_4 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_5 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_6 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_7 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_8 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_9 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_10 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_11 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_12 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_13 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_14 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_15 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_16 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_17 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_18 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_19 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_20 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_21 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_22 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_23 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_24 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_25 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_26 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_27 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_28 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_29 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_30 (interrupt_frame_s *frame) __attribute__ ((interrupt));
void isr_vector_31 (interrupt_frame_s *frame) __attribute__ ((interrupt));

#endif /* __ISR_H__ */

