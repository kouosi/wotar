#ifndef __GDT_H__
#define __GDT_H__

#include <stdint.h>

typedef struct {
    uint16_t limit_lower;
    uint16_t base_lower;
    uint8_t base_middle;
    uint8_t access_byte;
    uint8_t granularity;
    uint8_t base_high;
} __attribute__ ((packed)) gdt_entry_s;

typedef struct {
    uint16_t limit;
    uintptr_t base;
} __attribute__ ((packed)) gdt_ptr_s;

void gdt_init (void);

#endif /* __GDT_H__ */

