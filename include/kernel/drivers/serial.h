#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <kernel/system.h>

#define PORT_COM1 0x3F8

void serial_init (void);
uint8_t serial_read (void);
void serial_write (char ch);
uint32_t serial_received (void);
uint32_t is_transmit_empty (void);
void serial_puts (const char *str);

#endif /* __SERIAL_H__ */

