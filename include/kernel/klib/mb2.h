#ifndef __MB2_H__
#define __MB2_H__

#include <stdint.h>

/* Magic numbers */
#define MB2_HEADER_MAGIC     0xe85250d6
#define MB2_BOOTLOADER_MAGIC 0x36d76289

/* Multiboot 2 info request tags list */
#define MB2_HEADER_TAG_OPT           1
#define MB2_REQ_TAG_END              0
#define MB2_REQ_TAG_INFO_REQ         1
#define MB2_REQ_TAG_ADDR             2
#define MB2_REQ_TAG_ENTRY_ADDR       3
#define MB2_REQ_TAG_CMD_FLAGS        4
#define MB2_REQ_TAG_FRAMEBUFFER      5
#define MB2_REQ_TAG_MODULE_ALIGN     6
#define MB2_REQ_TAG_BOOT_SERVICES    7
#define MB2_REQ_TAG_EFI32_ENTRY_ADDR 8
#define MB2_REQ_TAG_EFI46_ENTRY_ADDR 9
#define MB2_REQ_TAG_RELOCATABLE      10

/* Multiboot 2 boot info tag types list */
#define MB2_TAG_END               0
#define MB2_TAG_CMD_LINE          1
#define MB2_TAG_BOOTLOADER_NAME   2
#define MB2_TAG_MODULES           3
#define MB2_TAG_BASIC_MEM_INFO    4
#define MB2_TAG_BOOT_DEVICE       5
#define MB2_TAG_MEM_MAP           6
#define MB2_TAG_VBE               7
#define MB2_TAG_FRAMEBUFFER       8
#define MB2_TAG_ELF_SYMBOLS       9
#define MB2_TAG_APM_TABLE         10
#define MB2_TAG_EFI32             11
#define MB2_TAG_EFI64             12
#define MB2_TAG_SMBIOS            13
#define MB2_TAG_OLD_ACPI          14
#define MB2_TAG_NEW_ACPI          15
#define MB2_TAG_NETWORK_INFO      16
#define MB2_TAG_EFI_MEM_MAP       17
#define MB2_TAG_EFI_BOOT_SERVICES 17
#define MB2_TAG_EFI32_IMG_HANDLER 19
#define MB2_TAG_EFI64_IMG_HANDLER 20
#define MB2_TAG_LOAD_BASE_ADDR    21

/* ARCH(32bit) */
#define MB2_ARCH_I686 0
#define MB2_ARCH_MIPS 4

/* Basic tags structure */
struct mb2_tag_struct {
    uint32_t type;
    uint32_t size;
};

/* Basic memory information */
struct mb2_tag_basic_mem_info {
    uint32_t type;
    uint32_t size;
    uint32_t mem_lower;
    uint32_t mem_upper;
};

/* BIOS Boot device */
struct mb2_tag_boot_device {
    uint32_t type;
    uint32_t size;
    uint32_t biosdev;
    uint32_t slice;
    uint32_t part;
};

/* Boot command line */
struct mb2_tag_cmd_line {
    uint32_t type;
    uint32_t size;
    uint8_t string[];
};

/* Modules */
struct mb2_tag_modules {
    uint32_t type;
    uint32_t size;
    uint32_t mod_start;
    uint32_t mod_end;
    uint8_t string[];
};

/* ELF-Symbols */
struct mb2_tag_elf_symbols {
    uint32_t type;
    uint32_t size;
    uint32_t num;
    uint32_t entsize;
    uint32_t shndx;
    uint8_t sections[];
};

#define MB2_MEM_TYPE_AVAILABLE   1
#define MB2_MEM_TYPE_USABLE_ACPI 3
#define MB2_MEM_TYPE_NVS         4
#define MB2_MEM_TYPE_DEFECTIVE   5

struct mb2_tag_mem_map_entry {
    uint64_t base_addr;
    uint64_t len;
    uint32_t type;
    uint32_t reserved;
};

/* Memory map */
struct mb2_tag_mem_map {

    uint32_t type;
    uint32_t size;
    uint32_t entry_size;
    uint32_t entry_version;
    struct mb2_tag_mem_map_entry entries[];
};

/* Boot loader name */
struct mb2_tag_bootloader_name {
    uint32_t type;
    uint32_t size;
    uint8_t string[];
};

/* APM(Advance Power Management) Table */
struct mb2_tag_apm_table {
    uint32_t type;
    uint32_t size;
    uint16_t version;
    uint16_t cseg;
    uint32_t offset;
    uint16_t cseg_16;
    uint16_t dseg;
    uint16_t flags;
    uint16_t cseg_len;
    uint16_t cseg_16_len;
    uint16_t dseg_len;
};

/* VBE(VESA BIOS EXTENSIONS) info */
struct mb2_tag_vbe {
    uint32_t type;
    uint32_t size;
    uint16_t vbe_mode;
    uint16_t vbe_interface_seg;
    uint16_t vbe_interface_off;
    uint16_t vbe_interface_len;
    uint8_t vbe_control_info[512];
    uint8_t vbe_mode_info[256];
};

#define MB2_FRAMEBUFFER_TYPE_INDEXED_COLOR 0
#define MB2_FRAMEBUFFER_TYPE_RGB_COLOR     1
#define MB2_FRAMEBUFFER_TYPE_EGA_TEXT      2

struct mb2_tag_framebuffer_type_rgb {
    uint8_t framebuffer_red_field_position;
    uint8_t framebuffer_red_mask_size;
    uint8_t framebuffer_green_field_position;
    uint8_t framebuffer_green_mask_size;
    uint8_t framebuffer_blue_field_position;
    uint8_t framebuffer_blue_mask_size;
};

struct mb2_tag_framebuffer_type_indexed {
    uint32_t framebuffer_palette_num_colors;

    struct {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    } *framebuffer_palette;
};

/* Framebuffer info */
struct mb2_tag_framebuffer {
    uint32_t type;
    uint32_t size;
    uint64_t framebuffer_addr;
    uint32_t framebuffer_pitch;
    uint32_t framebuffer_width;
    uint32_t framebuffer_height;
    uint8_t framebuffer_bpp;
    uint8_t framebuffer_type;
    uint8_t reserved;

    union color_info {
        struct mb2_tag_framebuffer_type_indexed _color_info_indexed;
        struct mb2_tag_framebuffer_type_rgb _color_info_rgb;
    } color_info;
};

/* EFI 32-bit system table pointer */
struct mb2_tag_efi32 {
    uint32_t type;
    uint32_t size;
    uint32_t pointer;
};

/* EFI 64-bit system table pointer */
struct mb2_tag_efi64 {
    uint32_t type;
    uint32_t size;
    uint64_t pointer;
};

/* SMBIOS tables */
struct mb2_tag_smbios {
    uint32_t type;
    uint32_t size;
    uint8_t major;
    uint8_t minor;
    uint8_t reserved[6];
    uint8_t simbios_tables[];
};

/* ACPI old (1.0) RSDP */
struct mb2_tag_old_acpi {
    uint32_t type;
    uint32_t size;
    uint8_t rsdp[];
};

/* ACPI new (2.0~) RSDP */
struct mb2_tag_new_acpi {
    uint32_t type;
    uint32_t size;
    uint8_t rsdp[];
};

/* Networking information */
struct mb2_tag_network_info {
    uint32_t type;
    uint32_t size;
    uint8_t dhcp_ack[];
};

/* EFI memory map */
struct mb2_tag_efi_mem_map {
    uint32_t type;
    uint32_t size;
    uint32_t desc_size;
    uint32_t desc_version;
    uint8_t efi_mem_map[];
};

/* EFI 32-bit image handle pointer */
struct mb2_tag_efi32_img_handler {
    uint32_t type;
    uint32_t size;
    uint32_t pointer;
};

/* EFI 64-bit image handle pointer */
struct mb2_tag_efi64_img_handler {
    uint32_t type;
    uint32_t size;
    uint64_t pointer;
};

/* Image load base physical address */
struct mb2_tag_load_base_addr {
    uint32_t type;
    uint32_t size;
    uint32_t load_base_addr;
};

/* Functions */
void parse_mb2_info (uint32_t mb2_addr);

#endif /* __MB2_H__ */

