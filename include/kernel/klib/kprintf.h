#ifndef __KPRINTF_H__
#define __KPRINTF_H__

typedef enum {
    kerr,
    kwarn,
    ksucc,
    kmsg
} kstream;

void kprintf (kstream print_stream, const char *fmt, ...);

#endif /* __KPRINTF_H__ */

